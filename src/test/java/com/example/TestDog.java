package com.example;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.jupiter.api.Test;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestDog {
    @Test
    public void testPrintDog() throws IOException {
        for (int i = 1; i <= 3; i++) {
            Dog dog = new Dog();
            String out = dog.GenDog(i);
            InputStream input = getClass().getResourceAsStream("/testdata/dog1-" + i + ".txt.golden");
            String golden = CharStreams.toString(new InputStreamReader(input, Charsets.UTF_8)).replaceAll("\\s+$", "");
            assertEquals(golden, out);
        }
    }
}
